import fs = require('fs');
//6526867 too low
//7274211 too low

//npm run compile && node dist/2015/day6/day-6-2.js src/2015/day6/input.txt

type state = number;
type op = 'on' | 'off' | 'toggle';

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let data = getInitialGrid();

		
		let instructions = input.split('\n').map(canonicalize);
		
		for (var instruction of instructions) {
			data = runInstruction(data, instruction);	
		}
		console.log(totalBrightness(data));
		
	});
}	

function canonicalize (input : string) : string[] {
	return input.replace('turn ', '').replace(' through', '').replace(',', ' ').replace(',', ' ').split(' ');
}

function getInitialGrid () : state[][] {
	const r = [];
	for (let x = 0; x < 1000; x ++) {
		let c : state[] = [];
		for (let y = 0; y < 1000; y ++) {
			c.push(0);
		}
		r.push(c);		
	}
	return r;
}

function runInstruction (data: state[][], instruction : any) : state[][] {
	let [op, startx, starty, endx, endy] : [op, number, number, number, number] = instruction;
	startx = Number(startx);
	starty = Number(starty);
	endx = Number(endx);
	endy = Number(endy);
	//this is much too complicated an not necessary, I misunderstood what was happening, but it works so im leaving it for now.
	for (let x = startx; (startx <= endx ? x <= endx : x >= endx); (startx < endx ? x++ : x--)) {
		for (let y = starty; (starty <= endy ? y <= endy : y >= endy); (starty < endy ? y++ : y--)) {
			let currentState: state = data[x][y];			
			if (op === 'on') {
				data[x][y] = currentState + 1;
			} else if (op === 'off') {
				data[x][y] = Math.max(0, currentState - 1);
			} else {
				data[x][y] = currentState + 2;
			}
		}
	}
	return data;
}

function totalBrightness (data: state[][]) : number {
	let sum = 0;
	for (let row of data) {
		for (let cell of row) {
			sum += cell;
		}
	}
	return sum;
}
