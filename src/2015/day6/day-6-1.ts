import fs = require('fs');
//387435 too low
//418954 too high

type state = 0 | 1;
type op = 'on' | 'off' | 'toggle';

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let data = getInitialGrid();

		//let instructions = [['toggle', '322', '558', '977', '958'], ['toggle', '322', '558', '977', '958']];
		let instructions = input.split('\n').map(canonicalize);
		//for (var instruction of instructions.slice(0, 10)) {
		for (var instruction of instructions) {
			data = runInstruction(data, instruction);
			//console.log(countOns(data));	
		}
		console.log(countOns(data));
		
	});
}	

function canonicalize (input : string) : string[] {
	return input.replace('turn ', '').replace(' through', '').replace(',', ' ').replace(',', ' ').split(' ');
}

function getInitialGrid () : state[][] {
	const r = [];
	for (let x = 0; x < 1000; x ++) {
		let c : state[] = [];
		for (let y = 0; y < 1000; y ++) {
			c.push(0);
		}
		r.push(c);		
	}
	return r;
}

function debug (data : state[][]) : void {
	for (let x in data) {
		console.log(data[x].join(' '));
	}
}

function flip (input: state) : state {
	if (input === 1) {
		return 0;
	}
	return 1;
}

function runInstruction (data: state[][], instruction : any) : state[][] {
	let [op, startx, starty, endx, endy] : [op, number, number, number, number] = instruction;
	startx = Number(startx);
	starty = Number(starty);
	endx = Number(endx);
	endy = Number(endy);
	//this is much too complicated an not necessary, I misunderstood what was happening, but it works so im leaving it for now.
	for (let x = startx; (startx <= endx ? x <= endx : x >= endx); (startx < endx ? x++ : x--)) {
		for (let y = starty; (starty <= endy ? y <= endy : y >= endy); (starty < endy ? y++ : y--)) {
			let currentState: state = data[x][y];
			
			if (op === 'on') {
				data[x][y] = 1;
			} else if (op === 'off') {
				data[x][y] = 0;
			} else {
				data[x][y] = flip(currentState);
			}
		}
	}
	return data;
}

function countOns (data: state[][]) : number {
	let count = 0;
	for (let row of data) {
		for (let cell of row) {
			if (cell === 1) {
				count += 1;
			}
		}
	}
	return count;
}
