import fs = require('fs');

const hasThreeVowels = function (input : string) : boolean {
	const vowels = new Set(['a','e','i','o','u']);
	var count = 0;
	for (let char of input.toLowerCase().split('')) {
		if (vowels.has(char)) {
			count += 1;
			if (count >= 3) {
				return true;
			}
		}
	}
	return false;
};

const hasDoubleLetter = function (input : string) : boolean {
	let lastLetter = undefined;
	for (let char of input.toLowerCase().split('')) {
		if (char === lastLetter) {
			return true;
		}
		lastLetter = char;
	}
	return false;
};

const hasBadString = function (input : string) : boolean {
	const badStrings = new Set(['ab', 'cd', 'pq', 'xy']);
	let lastLetter = undefined;
	for (let char of input.toLowerCase().split('')) {
		if (badStrings.has(lastLetter + char)) {
			return true;
		}
		lastLetter = char;
	}
	return false;
};

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, data: string) {
		console.log('nice strings', data.split('\n').filter(function (row : string) {
			row = row.trim();
			//console.log(row, hasThreeVowels(row), hasDoubleLetter(row), !hasBadString(row));
			return hasThreeVowels(row) && hasDoubleLetter(row) && !hasBadString(row);
		}).length);
	});
}	