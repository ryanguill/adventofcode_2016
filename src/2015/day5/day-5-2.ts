import fs = require('fs');

const hasPairTwice = function (input : string) : boolean {
	const pairs : string[] = [];
	let lastChar = undefined;
	for (let char of input.toLowerCase().split('')) {
		let currentPair = (lastChar || '') + char;
		let idx = pairs.findIndex(pair => pair === currentPair);
		//last part should protect against overlap
		if (idx !== -1 && idx !== pairs.length -1) {
			return true;
		}
		pairs.push(currentPair);
		lastChar = char;
	}
	return false;
};

const hasSandwich = function (input : string) : boolean {
	// eg xyx, or aaa
	let data = input.toLowerCase().split('');
	for (let i = 2; i < data.length; i++) {
		if (data[i] === data[i-2]) {
			return true;
		}
	}
	return false;
};

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, data: string) {
		console.log('nice strings', data.split('\n').filter(function (row : string) {
			row = row.trim();
			console.log(row, hasPairTwice(row), hasSandwich(row));
			return hasPairTwice(row) && hasSandwich(row);
		}).length);
	});
}	