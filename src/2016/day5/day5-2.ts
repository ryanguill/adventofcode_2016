import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');
import crypto = require('crypto');

// npm run compile && node dist/2016/day5/day5-2.js
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day5/day5-2.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	const input = 'reyedfim';
	let index = 0;
	let password : (string|undefined)[] = [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,];
	let foundCount = 0;
	while (true) {
		let hash = md5Hash(input + index.toString());
		if (hash.substring(0, 5) === '00000') {
			let pos = Number(hash.substring(5,6));
			if (!isNaN(pos) && pos >= 0 && pos <= 7 && password[pos] === undefined) {
				password[pos] = hash.substring(6, 7);
				foundCount += 1;
				process.stdout.write(`${hash} - ${index} - ${password.map(x => x ? x : '_').join('')}\r`);
			}
		}
		//un comment for a cool progress visualization, but it slows things down _a lot_
		//process.stdout.write(`${hash} - ${index} - ${password.map(x => x ? x : '_').join('')}\r`);
		if (foundCount === 8) {
			break;
		}
		index += 1;
	}
	process.stdout.write(`done! iterations: ${index}, password:${password.join('')}`);
}

function md5Hash (input : string) : string {
	return crypto.createHash('md5').update(input).digest('hex');
}



/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('md5hash', async function () {
			assert.equal(md5Hash('xyz'), 'd16fb36f0911f878998c136191af705e');
		});

		
	});
}	
