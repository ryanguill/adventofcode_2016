import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');
import crypto = require('crypto');

// npm run compile && node dist/2016/day5/day5-1.js
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day5/day5-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	const input = 'reyedfim';
	let index = 0;
	let password = [];
	while (true) {
		let hash = md5Hash(input + index.toString());
		if (hash.substring(0, 5) === '00000') {
			password.push(hash.substring(5,6));
			console.log({hash, password});
		}
		if (password.length === 8) {
			break;
		}
		index += 1;
	}
	console.log(password.join(''));
}

function md5Hash (input : string) : string {
	return crypto.createHash('md5').update(input).digest('hex');
}



/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {
/*
		test('test example function', async function () {
			
		});
*/
		
	});
}	
