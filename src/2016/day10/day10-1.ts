import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day10/day10-1.js src/2016/day10/input.txt
// time (npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day10/day10-1.js && say done)

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
// 		input = `value 5 goes to bot 2
// bot 2 gives low to bot 1 and high to bot 0
// value 3 goes to bot 1
// bot 1 gives low to output 1 and high to bot 0
// bot 0 gives low to output 2 and high to output 0
// value 2 goes to bot 2`;


		const instructions = input.split('\n').map(parseInstruction);
		const inputs : Input[] = [];
		const bots : Bot[] = [];
		const outputs : Output[] = [];

		function getBot (id: number) {
			let bot = bots.find(bot => bot.id === id);
			if (!bot) {
				bot = new Bot(id);
				bots.push(bot);
			}
			return bot;
		}

		function getOutput (id: number) {
			let output = outputs.find(o => o.id === id);
			if (!output) {
				output = new Output(id);
				outputs.push(output);
			}
			return output;
		}

		function execute (orders: Order[]) {
			orders.forEach(function(order){
				if (order.type === 'OUTPUT') {
					getOutput(order.id).value = order.value;
				} else {
					let newOrders = getBot(order.id).addValue(order.value);
					//console.log({newOrders});
					execute(newOrders);
				}
			});
		}

		for (let inst of instructions.filter(i => i.type !== 'SETUP')) {
			if (inst.type !== 'SETUP') {
				let bot = getBot(inst.source);
				bot.high = inst.high;
				bot.low = inst.low;
			}
		}

		for (let inst of instructions.filter(i => i.type === 'SETUP')) {
			if (inst.type === 'SETUP') {
				if (inst.dest.type === 'BOT') {
					let bot = getBot(inst.dest.id);
					inputs.push(new Input(inst.value, bot));
					let orders = bot.addValue(inst.value);
					//console.log({orders});
					execute(orders);
				}
			}	
		}	
/*
		for (var o of outputs.sort((a, b) => a.id - b.id)) {
		 	console.log({o});
		}

		for (var b of bots.sort((a, b) => a.id - b.id)) {
		 	console.log({b});
		}
*/
		//part 1
		console.log({answer: bots.filter(bot => {
			let [a, b] = bot.getValues();
			return a === 61 && b === 17;
		}).map(bot => bot.id)});		
		
		//part 2
		console.log({part2: outputs.filter(output => output.id < 3).map(output => output.value).reduce((product, factor) => product * factor)});

	});
}


type Inst = SetupInst | BotInst;

type SetupInst = {
	type : 'SETUP';
	value: number;
	dest: Node;
};

type BotInst = {
	type : 'BOT';
	source : number;
	low : Node;
	high : Node;
};

type Node = {
	type: 'BOT' | 'OUTPUT',
	id: number;
};

function parseInstruction (input : string) : Inst  {
	// value 43 goes to bot 90
	// bot 160 gives low to bot 197 and high to bot 202
	// bot 168 gives low to output 7 and high to bot 126
	const parts = input
		.replace('gives low to ', '')
		.replace('and high to ', '')
		.replace('value ', '')
		.replace('goes to bot ', '')
		.split(' ');
	// 43, 90
	// bot, 160, bot, 197, bot, 202
	// bot, 168, output, 7, bot 126
	if (parts.length === 2) {
		let [value, bot] = parts;
		return {
			type: 'SETUP',
			value: Number(value),
			dest: {
				type: 'BOT', 
				id: Number(bot)
			},
		};
	} else {
		let [_, sourceBot, lowType, lowID, highType, highID] = parts;
		return {
			type: 'BOT',
			source: Number(sourceBot),
			low: {
				type: lowType === 'bot' ? 'BOT' : 'OUTPUT',
				id: Number(lowID),
			},
			high: {
				type: highType === 'bot' ? 'BOT' : 'OUTPUT',
				id: Number(highID)
			},
		};
	}
}

type Order = {
	type: Node['type'];
	id: number;
	value: number;
};
class Bot {
	id : number;
	private _low? : Node;
	private _high? : Node;
	constructor (id : number) {
		this.id = id;
	}

	values : number[]  = [];
	addValue (value : number) : Order[] {
		this.values.push(value);
		return this.execute();
	}

	getValues () : [number, number] {
		const [a, b] = this.values.sort((a, b) => b - a);
		return [a, b];
	}

	set high (value : Node) {
		this._high = value;
	}

	set low (value : Node) {
		this._low = value;
	}

	isReady () : boolean {
		return this._low !== undefined && this._high !== undefined && this.values.length === 2;
	}

	execute () : Order[] {
		if (this._low !== undefined && this._high !== undefined && this.values.length === 2) {
			let [a, b] = this.getValues();
			return [
				{id: this._high.id, type: this._high.type, value: a}, 
				{id: this._low.id, type: this._low.type, value: b}
			];
		}
		return [];
	}
}

class Input {
	value: number;
	bot: Bot;
	constructor (value : number, bot : Bot) {
		this.value = value;
		this.bot = bot;
	}
}

class Output {
	id : number;
	value : number;
	constructor (id : number) {
		this.id = id;
	}
}



/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('parseInstruction', async function () {
			assert.deepEqual(parseInstruction('value 43 goes to bot 90'), {
				type: 'SETUP',
				value: 43,
				dest: {
					type: 'BOT',
					id: 90,
				},
			});
			assert.deepEqual(parseInstruction('bot 160 gives low to bot 197 and high to bot 202'), {
				type: 'BOT',
				source: 160,
				low: {
					type: 'BOT',
					id: 197,
				},
				high: {
					type: 'BOT',
					id: 202
				}
			});
			assert.deepEqual(parseInstruction('bot 168 gives low to output 7 and high to bot 126'), {
				type: 'BOT',
				source: 168,
				low: {
					type: 'OUTPUT',
					id: 7,
				},
				high: {
					type: 'BOT',
					id: 126
				}
			});
		});

		
	});
}	
