import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');

import {deepClone} from '../utils/utils';

// time (npm run compile && node dist/2016/day11/day11-1.js && say done)
// time (npm run compile && node --prof dist/2016/day11/day11-1.js && say done)
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day11/day11-1.js

// profiling
// npm run compile && node --prof dist/2016/day11/day11-1.js
// node --prof-process isolate-0x102004600-v8.log > prof-processed.txt

// part 1 1m35.467s = 31 // after some optimizations, 7.561s //more optimizations 5.385s
// part 2 6m41 = 55 // after optimizations 56.002s

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	/*
	`The first floor contains a thulium generator, a thulium-compatible microchip, a plutonium generator, and a strontium generator.
	The second floor contains a plutonium-compatible microchip and a strontium-compatible microchip.
	The third floor contains a promethium generator, a promethium-compatible microchip, a ruthenium generator, and a ruthenium-compatible microchip.
	The fourth floor contains nothing relevant.`;

	thulium = T
	plutonium = P
	strontium = S
	promethium = M
	ruthenium = R

	elerium = E
	dilithim = D
	*/

	/*
		start on floor 1, get everything to floor 4.
		must move at least one thing to move the elevator.
		cannot move more than two things.
		M must not be on a level with a different G, unless it has it's corresponding G
		get all parts up to level 4
	*/
	let part1 = {
		board: [
			['TG', 'TM', 'PG', 'SG'],
			['PM', 'SM'],
			['MG', 'MM', 'RG', 'RM'],
			[],
		],
		elevator: 0 as State['elevator']
	};

	let part2 = {
		board: [
			['TG', 'TM', 'PG', 'SG', 'EG', 'EM', 'DG', 'DM'],
			['PM', 'SM'],
			['MG', 'MM', 'RG', 'RM'],
			[],
		],
		elevator: 0 as State['elevator']
	};
	run(part1);
}

type Item = string;
type Floor = Item[];
type Board = Floor[];
type State = {
	board: Board,
	elevator : 0 | 1 | 2 | 3
};

/*
function validateFloor (floor : Floor) : boolean {
	//chip cannot be on the same level as any other gen without its gen
	let chips = floor.filter(item => item.charAt(1) === 'M');
	let gens = floor.filter(item => item.charAt(1) === 'G');
	for (let chip of chips) {
		let hasMatch = gens.filter(x => x.charAt(0) === chip.charAt(0)).length > 0;
		if (!hasMatch && gens.length) {
			return false;
		}
	}
	return true;
}
*/


//slightly faster, but much less readable version
function validateFloor (floor : Floor) : boolean {
	let chips = new Set();
	let gens = new Set();
	for (let item of floor) {
		let id = item.charAt(0);
		if (item.charAt(1) === 'M') {
			chips.add(id);
		} else {
			gens.add(id);
		}
	}
	if (gens.size === 0) {
		return true;
	}
	for (let chip of chips) {
		let hasMatch = gens.has(chip);
		if (!hasMatch) {
			return false;
		}
	}
	return true;
}


function validateLayout (board : Board) : boolean {
	return board.every(validateFloor);
}

function isWinningCondition (board : Board) : boolean {
	if (!validateLayout(board)) {
		return false;
	}
	//floor index 3 should have 10 elements, all others should have 0
	return board[0].length + board[1].length + board[2].length === 0 &&
			board[3].length > 1;
}

type Direction = 'UP' | 'DOWN';
type Move = [Item[], Direction];

function findPossibleMoves (state : State) : Move[] {
	let currentFloor = state.board[state.elevator];
	let pieceCombos = combine(currentFloor, 1, 2);
	
	if (state.elevator === 0) {
		//can only go up
		return pieceCombos.map(pieces => [pieces, 'UP' as Direction] as Move);
	} else if (state.elevator === 3) {
		//can only go down
		return pieceCombos.map(pieces => [pieces, 'DOWN' as Direction] as Move);
	} else {
		//can go up or down
		return pieceCombos.map(pieces => [pieces, 'UP' as Direction] as Move)
				.concat(pieceCombos.map(pieces => [pieces, 'DOWN' as Direction] as Move));
	}

}

function isDefined (value : any) : boolean {
	return value !== undefined;
}

function compactArray<T> (input : T[] ) : T[] {
	return input.filter(isDefined);
}

type ValidMove = {
	move: Move;
	newState: State;
};
function findValidMoves (state : State) : ValidMove[] {
	let possibleMoves = findPossibleMoves(state);
	
	let output = possibleMoves.reduce(function (agg, move) {
		let newState = applyMove(state, move);
		if (validateLayout(newState.board)) {
			agg.push({
				move,
				newState
			});
		}
		return agg;
	}, [] as ValidMove[]);

	//weed out sub-optimal moves

	//if all floors below you are empty, dont move anything down
	let emptyFloorsBelowMe = state.board.filter((floor, index) => index < state.elevator && floor.length === 0);
	if (state.elevator - emptyFloorsBelowMe.length === 0) {
		//remove all downs
		output = output.filter(move => move.move[1] !== 'DOWN');
	}

	//If you can move two items upstairs, don't bother bringing one item upstairs. 
	//If you can move one item downstairs, don't bother bringing two items downstairs. 
	//(But still try to move items downstairs even if you can move items upstairs)

	let twoItemUps = output.filter(move => move.move[1] === 'UP' && move.move[0].length === 2);
	if (twoItemUps.length) {
		output = output.filter(move => !(move.move[1] === 'UP' && move.move[0].length === 1));
	}

	let oneItemDowns = output.filter(move => move.move[1] === 'DOWN' && move.move[0].length === 1);
	if (oneItemDowns.length) {
		output = output.filter(move => !(move.move[1] === 'DOWN' && move.move[0].length === 2));
	}

	return output;
}

//http://stackoverflow.com/questions/5752002/find-all-possible-subset-combos-in-an-array
function combine<T> (a : T[], min: number, max: number) : T[][] {
    var fn = function(n : any, src: any[], got : any[], all : any[]) {
        if (n === 0) {
            if (got.length > 0) {
                all[all.length] = got;
            }
            return;
        }
        for (var j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
        }
        return;
    };
    var all : any[] = [];
    for (var i = min; i < a.length; i++) {
        fn(i, a, [], all);
    }
    all.push(a);
	all = all.filter(x => x.length <= max);
    return all;
}

function applyMove (state : State, move : Move) : State {
	let newState = deepClone(state);
	let [pieces, direction] = move;
	//remove the elements from the current floor
	newState.board[state.elevator] = newState.board[state.elevator].filter(item => !pieces.find(piece => piece === item));
	//find the new floor
	if (direction === 'UP') {
		newState.elevator = (state.elevator + 1 as State['elevator']);
	} else {
		newState.elevator = (state.elevator - 1 as State['elevator']);
	}
	newState.board[newState.elevator] = newState.board[newState.elevator].concat(pieces);

	return newState;
}

function dumpState (state : State) {
	state = deepClone(state);
	console.log(state.board.map((floor, index) => `${state.elevator === index ? '*' : ' '}  F${index+1}:  ${floor.join(' ')}`).reverse().join('\n'));
}

function hashState (state: State) : string {
	state = deepClone(state);
	let board = state.board.map(floor => floor.sort());
	let indexed : string[] = [];

	function getIndex(char : string) {
		let i = indexed.indexOf(char);
		if (i === -1) {
			indexed.push(char);
			return indexed.length-1;
		}
		return i;
	}

	return `E:${state.elevator}` + board.map(function (floor, index) {
		return `F${index}:` + floor.map(function (item) {
			let [char, type] = item.split('');
			return `${getIndex(char)}${type}`;
		}).join('');
	}).join('');
	
}

type Node = {
	distance: number;
	parent : Node | null;
	move: ValidMove;
	isWinningCondition: boolean;
	hash: string;
};

function run (state : State) {
	
	let root : Node = {
		distance: 0,
		parent : null,
		move: {newState: deepClone(state), move: [[], 'UP']}, //bogus move
		isWinningCondition: false,
		hash: hashState(state)
	};

	let queue : Node[] = [];
	queue.push(root);
	let history : Set<String> = new Set();
	history.add(root.hash);

	let winners : Node[] = [];

	let lastDebugOutput = 0;

	while (queue.length > 0) {
		let current = queue.shift()!;
		
		//if (current.distance === 1) break;
		let validMoves = findValidMoves(current.move.newState);
		/* debugging
		validMoves.forEach(validMove => {
			console.log(validMove.move);
			dumpState(validMove.newState)
		});
		*/
		

		let newNodes : Node[] = validMoves.reduce((agg, validMove) => {
			let hash = hashState(validMove.newState);
			
			if (!history.has(hash)) {
				if (history.size - lastDebugOutput > 100) {
					console.log(`${current.distance + 1} - history: ${history.size}\r`);
					lastDebugOutput = history.size;
				}	
				let node = {
					distance: current.distance + 1,
					parent: current,
					move: validMove,
					isWinningCondition: isWinningCondition(validMove.newState.board),
					hash: hash,
				};
				//dumpNode(node);
				if (node.isWinningCondition) {
					console.log('winner'!);
					dumpNode(node, false);
					winners.push(node);
				}

				history.add(hash);
				agg.push(node);
			}
			

			return agg;
		}, [] as Node[]);

		if (winners.length) {
			break;
		}

		Array.prototype.push.apply(queue, newNodes);			
	}

	console.log('winners', winners.length);
	winners.map(w => dumpNode(w, false));
}

function dumpNode (node: Node, recursive: boolean = false) {
	console.log({
		distance: node.distance,
		move: node.move.move,
		isWinningCondition: node.isWinningCondition,
		//parent: node.parent
	});
	dumpState(node.move.newState);
	if (recursive && node.parent) {
		dumpNode(node.parent, true);
	}
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('validateLayout', async function () {
			let testIndex = 0;
			assert.isTrue(validateFloor(['TG', 'TM', 'PG', 'SG']), `${++testIndex}`);
			assert.isTrue(validateFloor(['TM']), `${++testIndex}`);
			assert.isTrue(validateFloor(['TG']), `${++testIndex}`);
			assert.isTrue(validateFloor(['TG', 'SG']), `${++testIndex}`);
			assert.isTrue(validateFloor(['TM', 'MM']), `${++testIndex}`);
			assert.isTrue(validateFloor(['TM', 'MM', 'SM']), `${++testIndex}`);
			assert.isTrue(validateFloor(['TM', 'TG', 'SG']), `${++testIndex}`);
			assert.isFalse(validateFloor(['TM', 'SG']), `${++testIndex}`);
			assert.isFalse(validateFloor(['TM', 'TG', 'SM']), `${++testIndex}`);
			assert.isFalse(validateFloor(['HG','HM','LM']));
			assert.isTrue(validateFloor(['HM','LG','HG']));
		});

		test('findPossibleMoves', async function () {
			findPossibleMoves({board: [
				['PG', 'TG'],
				['PG', 'TG'],
				['PG', 'TG']
			], elevator: 1});
		});

		test('applyMove', async function () {
			let s = {
				board: [
					['TG', 'TM', 'PG', 'SG'],
					['PM', 'SM'],
					['MG', 'MM', 'RG', 'RM'],
					[],
				],
				elevator: 1 as State['elevator']
			};

			//dumpState(applyMove(s, [['PM'], 'DOWN']));
		});

		test('isWinningCondition', async function () {
			assert.isFalse(isWinningCondition([
					['TG', 'TM', 'PG', 'SG'],
					['PM', 'SM'],
					['MG', 'MM', 'RG', 'RM'],
					[],
				]));
			assert.isTrue(isWinningCondition([
					[],
					[],
					[],
					['TG', 'TM', 'PG', 'SG', 'PM', 'SM', 'MG', 'MM', 'RG', 'RM'],
				]));
			assert.isTrue(isWinningCondition([
					[],
					[],
					[],
					['LG','HG','HM','LM'],
				]));	
		});

		test('hashState', async function () {
			// let s = {
			// 	board: [
			// 		['TG', 'TM', 'PG', 'SG'],
			// 		['PM', 'SM'],
			// 		['MG', 'MM', 'RG', 'RM'],
			// 		[],
			// 	],
			// 	elevator: 1 as State['elevator']
			// };
			//console.log(hashState(s));

			let s1 = {
				board: [
					['AG'],
					['AM'],
					['BG', 'BM'],
					[],
				],
				elevator: 1 as State['elevator']
			};

			let s2 = {
				board: [
					['BG'],
					['BM'],
					['AG', 'AM'],
					[],
				],
				elevator: 1 as State['elevator']
			};

			let s3 = {
				board: [
					['BG'],
					['BM'],
					['AM', 'AG'],
					[],
				],
				elevator: 1 as State['elevator']
			};

			
			

			assert.isTrue(hashState(s1) === hashState(s2));
			assert.isTrue(hashState(s1) === hashState(s3));

			let s4 = {
				board: [
					['BM'],
					['BG'],
					['AM', 'AG'],
					[],
				],
				elevator: 1 as State['elevator']
			};

			assert.isFalse(hashState(s1) === hashState(s4));

			let s5 = {
				board: [
					['AM', 'AG', 'BM', 'BG'],
					[],
					[],
					[],
				],
				elevator: 1 as State['elevator']
			};

			let s6 = {
				board: [
					['BG', 'BM', 'AG', 'AM'],
					[],
					[],
					[],
				],
				elevator: 1 as State['elevator']
			};

			assert.isTrue(hashState(s5) === hashState(s6));

			let s = {
				board: [
					['TG', 'TM', 'PG', 'SG'],
					['PM', 'SM'],
					['MG', 'MM', 'RG', 'RM'],
					[],
				],
				elevator: 0 as State['elevator']
			};

			assert.equal(hashState(s), 'E:0F0:0G1G2G2MF1:0M1MF2:3G3M4G4MF3:');

		});

		// test('run', async function () {
		// 	let s = {
		// 		board: [
		// 			['TG', 'TM', 'PG', 'SG'],
		// 			['PM', 'SM'],
		// 			['MG', 'MM', 'RG', 'RM'],
		// 			[],
		// 		],
		// 		elevator: 0 as State['elevator']
		// 	};

		// 	// let s = {
		// 	// 	board: [
		// 	// 		['HM', 'LM'],
		// 	// 		['HG'],
		// 	// 		['LG'],
		// 	// 		[],
		// 	// 	],
		// 	// 	elevator: 0 as State['elevator']
		// 	// };	

		// 	// let s = {
		// 	// 	board: [
		// 	// 		[],
		// 	// 		[],
		// 	// 		['HM', 'LM'],
		// 	// 		['LG', 'HG'],
		// 	// 	],
		// 	// 	elevator: 2 as State['elevator']
		// 	// };	

		// 	run(s);
		// });
		
	});
}	
