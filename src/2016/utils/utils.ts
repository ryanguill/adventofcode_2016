import mocha = require('mocha');
import chai = require('chai');

import crypto = require('crypto');

let expect = chai.expect;
let assert = chai.assert;

// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/utils/utils.js

const runningUnitTests = () => process.argv.length > 1 && process.argv[1].includes('mocha');
const getUnitTestLabel = () => `${__filename.split('/').reverse()[0]} tests`;
const testSuite = (f : (() => void), label : string = getUnitTestLabel()) => runningUnitTests() && suite(label, f);

export function combine<T> (input : T[], min : number, max : number) : T[][] {
	var out : T[][] = [];
	min = Math.max(min, 0);
	max = Math.max(min, Math.min(max, input.length));

	const fn = function (n : number, src: T[], acc : T[], out : T[][])  {
		if (n === 0) {
			if (acc.length > 0) {
				out[out.length] = acc;
			}
			return;
		}
		for (let j = 0; j < src.length; j++) {
			fn(n - 1, src.slice(j + 1), acc.concat([src[j]]), out);
		}
		return;
	};

	for (let i = min; i <= max; i++) {
		fn(i, input, [], out);
	}
	return out;
}

	testSuite(function () {
		test('combine', async function () {
			assert.deepEqual(combine([1,2,3], 1, 1), [ [1], [2], [3] ]);
			assert.deepEqual(combine([1,2,3], 1, 2), [ [1], [2], [3], [1,2], [1,3], [2,3] ]);
			assert.deepEqual(combine([1,2,3], 2, 2), [ [1,2], [1,3], [2,3] ]);
			assert.deepEqual(combine([1,2,3], 1, 3), [ [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3] ]);
			assert.deepEqual(combine([1,2,3], 3, 3), [ [1,2,3] ]);
			assert.deepEqual(combine([1,2,3], 3, 10), [ [1,2,3] ]);
			assert.deepEqual(combine([1,2,3], -10, 10), [ [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3] ]);
		});		
	});
	

export type Point = [number, number];
export function manhattanDistance (p1 : Point, p2 : Point) : number {
	return Math.abs(p1[0] - p2[0]) + Math.abs(p1[1] - p2[1]);
}

	testSuite(function () {
		test('manhattanDistance', async function () {
			assert.equal(manhattanDistance([1,1], [2,2]), 2);
			assert.equal(manhattanDistance([0,0], [0,10]), 10);
			assert.equal(manhattanDistance([0,0], [10,0]), 10);
		});		
	});


export function deepClone<T> (input : T) : T {
	return JSON.parse(JSON.stringify(input));
}


	testSuite(function () {
		test('deepClone', async function () {
			assert.deepEqual(deepClone({a: 1, b: 2, c: 3}), {a: 1, b: 2, c: 3});
			//more tests needed
		});		
	});


//currying signatures
export function take<T> (count : number) : (x : T[]) => T[];
export function take<T> (count : number, input : T[]) : T[];
export function take<T> (count : number, input? : T[]) {
	if (!input) {
		return (x : T[]) : T[] => take(count, x);
	}
	return input.slice(0, Math.max(0, count));
}

	testSuite(function () {
		test('take', async function () {
			// tbd
		});	
	});

//currying signatures
export function pluck (key : string) : (x : Object) => any;
export function pluck (key : string, input : {[key: string]: any}) : any;
export function pluck (key : string, input? : {[key: string]: any}) : any | undefined {
	if (!input) {
		return function (input : {[key: string]: any}) : any | undefined {
			return pluck(key, input);
		};
	}
	if (input.hasOwnProperty(key)) {
		return input[key];
	}
	return undefined;
}

	testSuite(function () {
		test('pluck', async function () {
			// tbd
		});	
	});

export function sum (a : number, b : number) {
	return a + b;
}

	testSuite(function () {
		test('sum', async function () {
			// tbd
		});	
	});

export function md5Hash (input : string) : string {
	return crypto.createHash('md5').update(input).digest('hex');
}

	testSuite(function () {
		test('md5Hash', async function () {
			// tbd
		});	
	});


export function md5HashStretched (input : string, count : number) : string {
	let hash = md5Hash(input);
	for (var i = 1; i < count; i++) {
		hash = md5Hash(hash);
	}
	return hash;
}

	testSuite(function () {
		test('md5HashStretched', async function () {
			// tbd
		});	
	});	

export function head<T> (input : T[]) : T {
	return input[0];
}

	testSuite(function () {
		test('head', async function () {
			assert.equal(head([1]), 1);
			assert.equal(head([2,3,4]), 2);
			assert.equal(head([]), undefined);
		});
	});

export function tail<T> (input : T[]) : T[] {
	return input.slice(1, input.length);
}

	testSuite(function () {
		test('tail', async function () {
			assert.deepEqual(tail([1,2,3,4]), [2,3,4]);
			assert.deepEqual(tail([1,2,3,4,5,6]), [2,3,4,5,6]);
			assert.deepEqual(tail([1]), []);
			assert.deepEqual(tail([]), []);
		});	
	});

export function equal<T> (input : T) : (x : T) => boolean;
export function equal<T> (input : T, test : T) : boolean;
export function equal<T> (input : T, test? : T) {
	if (test) {
		return input === test;
	} else {
		return function (test : T) : boolean {
			return input === test;
		};
	}	
}

	testSuite(function () {
		test('equal', async function () {
			assert.isFunction(equal('foo'));
			assert.isTrue(equal('foo')('foo'));
			assert.isTrue(equal('foo', 'foo'));
			assert.isFalse(equal('foo')('bar'));
			assert.isFalse(equal('foo', 'bar'));
		});	
	});

export function trim (input : string)  {
	return input.trim();
}

	testSuite(function () {
		test('trim', async function () {
			// tbd
		});	
	});

type TimesF = (i : number) => any;
export function times (count : number) : (x : TimesF) => any[];
export function times (count : number, f : TimesF) : any[];
export function times (count : number, f? : TimesF) : ((x : TimesF) => any[]) | any[] {
	if (!f) {	
		return function (f : TimesF) : any[] {
			return times(count, f);
		};
	}
	const output = [];
	for (let i = 0; i < count; i++) {
		output.push(f(i));
	}
	return output;
}	

	testSuite(function () {
		test('times', async function () {
			// tbd
		});	
	});

export function rotateArray<T> (input : T[], backwards : boolean = false) : T[] {
	let out = deepClone(input);
	if (out.length === 0) {
		return out;
	}
	if (backwards) {
		let x = out.shift();
		out.push(x!);
		
	} else {
		let x = out.pop();
		out.unshift(x!);
	}
	return out;
}		

	testSuite(function () {
		test('rotateArray<T>', async function () {
			assert.deepEqual(rotateArray([]), []);
			assert.deepEqual(rotateArray([1]), [1]);
			assert.deepEqual(rotateArray([1,2]), [2,1]);
			assert.deepEqual(rotateArray([1,2,3]), [3,1,2]);
			assert.deepEqual(rotateArray([1,2,3,4]), [4,1,2,3]);


			assert.deepEqual(rotateArray([], true), []);
			assert.deepEqual(rotateArray([1], true), [1]);
			assert.deepEqual(rotateArray([1,2], true), [2,1]);
			assert.deepEqual(rotateArray([1,2,3], true), [2,3,1]);
			assert.deepEqual(rotateArray([1,2,3,4], true), [2,3,4,1]);
		});	
	});

function transpose<T> (input : T[][]) : T[][] {
	return input[0].map((x, i) => input.map(y => y[i]));
}

	testSuite(function () {
		test('transpose', async function () {
			let input = [
				[1, 1, 1, 0],
				[0, 1, 0, 0],
				[0, 0, 1, 0],
				[0, 1, 1, 1],
			];

			assert.deepEqual(transpose(input), [
				[1, 0, 0, 0],
				[1, 1, 0, 1],
				[1, 0, 1, 1],
				[0, 0, 0, 1],
			]);
			
		});	
	});

export function countOnBitsInNumber(input: number) : number {
	let x = Math.floor(input); //make sure we have an int
	let count: number = x - ((x >> 1) & 0o33333333333) - ((x >> 2) & 0o11111111111);
	return ((count + (count >> 3)) & 0o30707070707) % 63;
}

	testSuite(function () {
		test('countOnBitsInNumber', async function () {
			assert.equal(countOnBitsInNumber(42), 3);
			assert.equal(countOnBitsInNumber(0), 0);
			assert.equal(countOnBitsInNumber(1), 1);
			assert.equal(countOnBitsInNumber(2), 1);
			assert.equal(countOnBitsInNumber(3), 2, Number(3).toString(2));
			assert.equal(countOnBitsInNumber(4), 1, Number(4).toString(2));

		});
	});


export function getNeighbors ([x, y] : Point) : Point[] {
	return [
		[x, y + 1],
		[x + 1, y],
		[x, y - 1],
		[x - 1, y]
	];
}

	testSuite(function () {
		test('countOnBitsInNumber', async function () {
			assert.deepEqual(getNeighbors([1,1]), [
				[1,2],
				[2,1],
				[1,0],
				[0,1],
			]);

			assert.lengthOf(getNeighbors([1,1]), 4);

		});
	});
