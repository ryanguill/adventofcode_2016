import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');

// time (npm run compile && node dist/2016/day12/day12-1.js src/2016/day12/input.txt && say done)
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day12/day12-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let instructions = input.split('\n').map(parseInstruction);
		let state : State = {
			registers: {
				a: 0,
				b: 0,
				c: 1,
				d: 0,
			},
			currentIndex: 0,
		};
		let actualIterations = 0;
		for (var i = 0; i < instructions.length; i++) {
			actualIterations += 1;
			
			//console.log("before", {r: state.registers, ci: state.currentIndex, i: instructions[i]});
			state = runInstruction(state, instructions[i]);
			
			//console.log({r: state.registers, ni: state.currentIndex, i: instructions[i]});
			i = state.currentIndex - 1; //because it will be incremented in the loop
			
			if (actualIterations % 10000 === 0) {
				console.log(`${actualIterations}: ${'  '.substring(0, 2-i.toString().length) + i} / ${instructions.length} ${state.registers.a} ${state.registers.b} ${state.registers.c} ${state.registers.d}}\r`);
			}	

			// if (actualIterations > 20) {
			// 	break;
			// }
		}

		console.log(state);
	});
}

function runInstruction (state : State, inst: Inst) : State {
	let [f, ...args] = inst;
	return f.apply(null, ([state] as any).concat(args));
}

type CopyInst = [typeof copy, Register | Value, Register];
type IncInst = [typeof increment, Register];
type DecInst = [typeof decrement, Register];
type JnzInst = [typeof jnz, Register, Value];
type Inst = CopyInst | IncInst | DecInst | JnzInst;

function parseInstruction (input : string) : Inst {
	let [a, b, c] = input.trim().split(' ');
	if (a === 'cpy') {
		let from = isNaN(Number(b)) ? b as Register : Number(b);
		return [copy, from, c as Register];
	} else if (a === 'inc') {
		return [increment, b as Register];
	} else if (a === 'dec') {
		return [decrement, b as Register];
	} else if (a === 'jnz') {
		return [jnz, b as Register, Number(c) as Value];
	}
	throw `Invalid Instruction: ${a}`;
}


type Value = number;
type Registers = {
	a : number;
	b : number;
	c : number;
	d : number;
};
type Register = keyof Registers;
type State = {
	registers: Registers;
	currentIndex : number;
};

function copy (state : State, from : Register | Value, to : Register) : State {
	if (typeof from === 'number') {
		state.registers[to] = from;
	} else {
		state.registers[to] = state.registers[from];
	}
	state.currentIndex += 1;
	return state;
}

function increment (state : State, r : Register) : State  {
	state.registers[r] += 1;
	state.currentIndex += 1;
	return state;
}

function decrement (state : State, r : Register) : State  {
	state.registers[r] -= 1;
	state.currentIndex += 1;
	return state;
}

function jnz (state : State, check : Register, newPos : Value) : State {
	if (state.registers[check] !== 0) {
		state.currentIndex += newPos;
	} else {
		state.currentIndex += 1;
	}	
	return state;
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('jnz', async function () {
			let state : State = {
				registers: {
					a: 0,
					b: 0,
					c: 1,
					d: 0,
				},
				currentIndex: 10,
			};
			assert.equal(jnz(state, 'c', -2).currentIndex, 8);

			state = {
				registers: {
					a: 0,
					b: 0,
					c: 1,
					d: 0,
				},
				currentIndex: 10,
			};
			assert.equal(jnz(state, 'c', 2).currentIndex, 12);

			state = {
				registers: {
					a: 2,
					b: 1,
					c: 1,
					d: 25,
				},
				currentIndex: 15,
			};
			assert.equal(jnz(state, 'd', -6).currentIndex, 9);
		});

		
	});
}	
