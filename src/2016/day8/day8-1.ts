import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day8/day8-1.js src/2016/day8/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day8/day8-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let board = getBoard(50, 6);
		let instructions = input.split('\n').map(parseInstruction);
		board = instructions.reduce(function (board, inst){
			let [f, a, b] = inst;
			return f(board, a, b);
		}, board);

		console.log({answer: sumBoard(board)});
		console.log(drawBoard(board));
	});
}

enum Pixel {
	'ON' = 1,
	'OFF' = 0,
};
type Board = Pixel[][];

function getBoard (width : number, height : number) : Board {
	return [...Array(Math.max(height, 1))].map(() => Array( Math.max(width, 1)).fill(Pixel.OFF));
} 

type TimesF = (i : number) => any;
function times (count : number) : (x : TimesF) => any[];
function times (count : number, f : TimesF) : any[];
function times (count : number, f? : TimesF) : ((x : TimesF) => any[]) | any[] {
	if (!f) {	
		return function (f : TimesF) : any[] {
			return times(count, f);
		};
	}
	const output = [];
	for (let i = 0; i < count; i++) {
		output.push(f(i));
	}
	return output;
}

function deepClone (input : any) : any {
	return JSON.parse(JSON.stringify(input));
}

function rotateRow (board : Board, y : number, amount : number, backwards : boolean = false) : Board {
	let outBoard = deepClone(board);
	times(amount, () => {
		if (backwards) {
			outBoard[y].push(outBoard[y].shift());
		} else {
			outBoard[y].unshift(outBoard[y].pop());
		}	
	});
	return outBoard;
}

function rotateColumn (board : Board, x : number, amount : number) : Board {
	return transpose(rotateRow(transpose(deepClone(board)), x, amount));
}

function drawRect (board : Board, width : number, height : number) : Board {
	board = deepClone(board);
	times(Math.max(width, 1), function (y) {
		times(Math.max(height, 0), function (x){
			board[y][x] = Pixel.ON;
		});
	});

	return board;
}

function parseInstruction (input : string) : [Function, number, number] {
	let [a, b, c, d, e] = input.split(' ');
	if (a === 'rect') {
		let [w, h] = b.split('x');
		return [drawRect, Number(h), Number(w)];
	}
	// must be a rotate
	let pos = Number(c.split('=')[1]);

	if (b === 'row') {
		return [rotateRow, pos, Number(e)];
	}
	// must be a column
	return [rotateColumn, pos, Number(e)];
}

function sumBoard (board : Board) : number {
	return board.reduce(function (total, row) {
		return total += row.filter(p => p === Pixel.ON).length;
	}, 0);	
}

function transpose (board : Board) : Board {
	return board[0].map((x, i) => board.map(y => y[i]));
}

function drawBoard (board : Board, onChar : string = '◼️', offChar : string = '◻️') : string {
	return board.reduce(function (agg, row) {
		return agg + row.map(x => x ? onChar : offChar).join('') + '\n';
	}, '');
}

function debugBoard (board : Board) {
	console.log(drawBoard(board));
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('getBoard', async function () {
			let b = getBoard(10,2);
			assert.lengthOf(b, 2);
			assert.lengthOf(b[0], 10);

			b = getBoard(2, 10);
			assert.lengthOf(b, 10);
			assert.lengthOf(b[0], 2);

			// make sure theyre all off
			assert.equal(b.filter(x => x.filter(y => y !== Pixel.OFF).length !== 0), 0);
		});

		test('rotateRow', async function () {
			let b = [[1, 0, 0, 0, 0]];

			assert.deepEqual(rotateRow(b, 0, 1), [[0, 1, 0, 0, 0]]);
			assert.deepEqual(rotateRow(b, 0, 2), [[0, 0, 1, 0, 0]]);

			// backwards
			assert.deepEqual(rotateRow(b, 0, 1, true), [[0, 0, 0, 0, 1]]);
			assert.deepEqual(rotateRow(b, 0, 2, true), [[0, 0, 0, 1, 0]]);
		});

		test('rotateColumn', async function () {
			let b = [[1],[0],[0],[0],[0]];

			assert.deepEqual(rotateColumn(b, 0, 1), [[0],[1],[0],[0],[0]]);
			assert.deepEqual(rotateColumn(b, 0, 2), [[0],[0],[1],[0],[0]]);
		});

		test('drawRect', async function () {
			let b = getBoard(4,4);

			assert.deepEqual(drawRect(b, 2, 2), [
				[1, 1, 0, 0],
				[1, 1, 0, 0],
				[0, 0, 0, 0],
				[0, 0, 0, 0],
			]);

			assert.deepEqual(drawRect(b, 1, 3), [
				[1, 1, 1, 0],
				[0, 0, 0, 0],
				[0, 0, 0, 0],
				[0, 0, 0, 0],
			]);

			assert.deepEqual(drawRect(b, 3, 1), [
				[1, 0, 0, 0],
				[1, 0, 0, 0],
				[1, 0, 0, 0],
				[0, 0, 0, 0],
			]);

		});	

		test('parseInstruction', async function () {
			assert.deepEqual(parseInstruction('rect 7x1'), [drawRect, 1, 7]);
			assert.deepEqual(parseInstruction('rotate column x=34 by 2'), [rotateColumn, 34, 2]);
			assert.deepEqual(parseInstruction('rotate row y=2 by 18'), [rotateRow, 2, 18]);
		});

		test('sumBoard', async function () {
			let b = getBoard(5, 5);

			assert.equal(sumBoard(b), 0);
			assert.equal(sumBoard(drawRect(b, 2, 2)), 4);
		});

		test('transpose', async function () {
			let board = [
				[1, 1, 1, 0],
				[0, 1, 0, 0],
				[0, 0, 1, 0],
				[0, 1, 1, 1],
			];

			assert.deepEqual(transpose(board), [
				[1, 0, 0, 0],
				[1, 1, 0, 1],
				[1, 0, 1, 1],
				[0, 0, 0, 1],
			]);
			
		});
		
	});
}	
