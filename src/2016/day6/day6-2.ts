import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day6/day6-2.js src/2016/day6/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day6/day6-2.js
//pdesmnoz

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		
		let answer = input
			.split('\n')
			.map(line => line.trim().split(''))
			.reduce(function (totals, line) {
				return line.reduce(function (totals, char, index) {
					let counts = totals[index] || [];
					let occurance = counts.find(x => equal(pluck('char', x), char));
					if (occurance) {
						occurance.count += 1;
					} else {
						counts.push({char, count:1});
					}
					totals[index] = counts;
					return totals;
				}, totals);
			}, <Totals>[])
			.map(counts => counts.sort((b, a) => b.count - a.count))
			.map(head)
			.map(pluck('char'))
			.join('');

		console.log({answer});	
	});
}

type Occurance = {
	char: string;
	count: number;
};
type OccurranceCount = Occurance[];
type Totals = OccurranceCount[];

function pluck (key : string) : (x : Object) => any;
function pluck (key : string, input : {[key: string]: any}) : any;
function pluck (key : string, input? : {[key: string]: any}) : any | undefined {
	if (!input) {
		return function (input : {[key: string]: any}) : any | undefined {
			return pluck(key, input);
		};
	}
	if (input.hasOwnProperty(key)) {
		return input[key];
	}
	return undefined;
}

function head<T> (input : T[]) : T {
	return input[0];
}

function equal<T> (input : T) : (x : T) => boolean;
function equal<T> (input : T, test : T) : boolean;
function equal<T> (input : T, test? : T) {
	if (test) {
		return input === test;
	} else {
		return function (test : T) : boolean {
			return input === test;
		};
	}
	
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('equal', async function () {
			assert.isFunction(equal('foo'));
			assert.isTrue(equal('foo')('foo'));
			assert.isTrue(equal('foo', 'foo'));
			assert.isFalse(equal('foo')('bar'));
			assert.isFalse(equal('foo', 'bar'));
		});

		test('head', async function () {
			assert.equal(head([1]), 1);
			assert.equal(head([2,3,4]), 2);
			assert.equal(head([]), undefined);
		});
		
	});
}	
