import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day6/day6-1.js src/2016/day6/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day6/day6-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		
		let totals: Totals = [];
		input
			.split('\n')
			.map(line => line.trim().split(''))
			//need to come back and figure out how to do this better, probably a reduce
			.forEach(function (line) {
				line.forEach(function (char, index) {
					let counts = totals[index] || [];
					let occurance = counts.find(x => x.char === char);
					if (occurance) {
						occurance.count += 1;
					} else {
						counts.push({char, count:1});
					}
					totals[index] = counts;
				});
			});

		let answer = totals.map(function(counts) {
			return counts.sort((a, b) => b.count - a.count);
		})
			//.forEach(x => console.log(x));
			.map(head)
			.map(pluck('char')).join('');
		console.log({answer});	

	});
}

type OccurranceCount = {
	char: string;
	count: number;
}[];

type Totals = OccurranceCount[];


function pluck (key : string) : (x : Object) => any;
function pluck (key : string, input : {[key: string]: any}) : any;
function pluck (key : string, input? : {[key: string]: any}) : any | undefined {
	if (!input) {
		return function (input : {[key: string]: any}) : any | undefined {
			return pluck(key, input);
		};
	}
	if (input.hasOwnProperty(key)) {
		return input[key];
	}
	return undefined;
}

function head<T> (input : T[]) : T {
	return input[0];
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {
/*
		test('test example function', async function () {
			
		});
*/

		
	});
}	
