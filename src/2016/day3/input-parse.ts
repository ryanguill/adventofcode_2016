import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');

let expect = chai.expect;
let assert = chai.assert;

// npm run compile && node dist/2016/day3/input-parse.js src/2016/day3/original-input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day3/input-parse.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let lines = input.split('\n').map(function(line) : any {
			return <String[]>line.trim().split(' ').filter((x:string) => x.length > 0);	
		}).map((input:string[]) => input.map((x:string) => Number(x)))
		.filter(function(input:number[]){
			let [x,y,z] = input;
			return isValidTriangle(x,y,z);
		});

		console.log(lines.length);

	});
}

function isValidTriangle (x : number, y : number, z : number) : boolean {
	return x + y > z && y + z > x && z + x > y;
}


/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
	suite(__filename.split('/').reverse()[0] + ' tests', function () {
/*
		test('test example function', async function () {
			
		});
*/
		
	});
}	
