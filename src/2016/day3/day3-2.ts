import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');

// npm run compile && node dist/2016/day3/day3-2.js src/2016/day3/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day3/day3-2.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let lines = input.split('\n').map(function(line) : any {
			return line.split(',');	
		}).map(function(line) : Line {
			let [x, y, z] = line;
			return {x:Number(x), y:Number(y), z:Number(z)};
		});

		type Line = {x:number, y:number, z:number};
		let empty : Line = {x:0, y:0, z:0};
		let index = 0;
		let goodCount = 0;
		let line1 : Line= empty, line2: Line = empty, line3: Line = empty;
		for (let line of lines) {
			index++;
			let m = index % 3;
			if (m === 1) {
				line1 = line;
			} else if (m === 2) {
				line2 = line;
			} else {
				line3 = line;

				if (isValidTriangle(line1.x, line2.x, line3.x)) {
					goodCount++;
				}
				if (isValidTriangle(line1.y, line2.y, line3.y)) {
					goodCount++;
				}
				if (isValidTriangle(line1.z, line2.z, line3.z)) {
					goodCount++;
				}
			}
		}
		console.log(goodCount);
	});
}

function isValidTriangle (x : number, y : number, z : number) : boolean {
	return x + y > z && y + z > x && z + x > y;
}


/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {

	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {
/*
		test('test example function', async function () {
			
		});
*/
		
	});
}	
