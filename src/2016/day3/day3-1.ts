import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');

// npm run compile && node dist/2016/day3/day3-1.js src/2016/day3/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day3/day3-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let lines = input.split('\n').map(function(line) : any {
			return line.split(',');
			
		}).filter(function(inputs) {
			let [x, y, z] : [string, string, string] = inputs;
			return isValidTriangle(Number(x), Number(y), Number(z));
		});

		console.log(lines.length);

	});
}

function isValidTriangle (x : number, y : number, z : number) : boolean {
	return x + y > z && y + z > x && z + x > y;
}


/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {

	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('isValidTriangle', async function () {
			assert.isTrue(isValidTriangle(2,3,4));
			assert.isFalse(isValidTriangle(2,2,5));
		});

		
	});
}	
