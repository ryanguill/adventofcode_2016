import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');

import {countOnBitsInNumber, getNeighbors, Point} from '../utils/utils';

// time (npm run compile && node dist/2016/day13/day13-1.js src/2016/day13/input.txt && say done)
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day13/day13-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/

if (process.argv.length > 1 && process.argv[1] === __filename) {
	bfs(1364);
}

enum Tile {
	OPEN = 0, WALL = 1
}

function getTile (puzzleInput : number) : ((x : Point) => Tile);
function getTile (puzzleInput : number, [x, y] : Point) : Tile;
function getTile (puzzleInput : number, [x, y] : Point | [undefined, undefined] = [undefined, undefined]) : Tile | ((x:Point) => Tile) {
	if (x === undefined || y === undefined) {
		return function ([x, y] : Point) : Tile {
			return getTile(puzzleInput, [x, y]);
		};
	}

	return countOnBitsInNumber((x*x + 3*x + 2*x*y + y + y*y) + puzzleInput) % 2 === 0 ? 0 : 1;
}

type Node = {
	point : Point;
	parent : Node | null;
	distance : number;
};

function bfs (puzzleInput : number) {
	console.log('starting...');
	const queue : Node[] = [];
	queue.push({
		point : [1,1],
		parent : null,
		distance: 0
	});
	const history : Set<string> = new Set();

	const getTileX = getTile(puzzleInput);

	let lastDebugOutput = 0;
	let winners : Node[] = [];

	while (queue.length > 0) {
		let current = queue.shift()!;

		let moves = getNeighbors(current.point).filter(p => getTileX(p) === 0);
		//console.log(moves, moves.map(getTileX), history);
		let newNodes : Node[] = moves.reduce((agg, point) => {
			let [x, y] = point;
			const hash = `${x}:${y}`;
			if (!history.has(hash)) {
				//console.log(history.size, point);
				if (history.size - lastDebugOutput > 100) {
					console.log(`${current.distance + 1} - history: ${history.size}\r`);
					lastDebugOutput = history.size;
				}

				let node = {
					point,
					parent: current,
					distance: current.distance + 1
				};

				if (x === 31 && y === 39) {
					console.log('winner!');
					console.log(node);
					winners.push(node);
				}

				history.add(hash);
				agg.push(node);
			}
			return agg;
		}, [] as Node[]);

		//console.log({newNodes});

		if (winners.length) {
			break;
		}

		Array.prototype.push.apply(queue, newNodes);
	}

}



/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('getTile', async function () {
			assert.equal(getTile(10, [0, 0]), Tile.OPEN);
			assert.equal(getTile(10, [1, 0]), Tile.WALL);
			assert.equal(getTile(10, [2, 0]), Tile.OPEN);
			assert.equal(getTile(10, [3, 0]), Tile.WALL);
			assert.equal(getTile(10, [7, 0]), Tile.OPEN);
			assert.equal(getTile(10, [2, 1]), Tile.WALL);

			let getTile10 = getTile(10);
			assert.equal(getTile10([0, 0]), Tile.OPEN);
			assert.equal(getTile10([1, 0]), Tile.WALL);
			assert.equal(getTile10([2, 0]), Tile.OPEN);
			assert.equal(getTile10([3, 0]), Tile.WALL);
			assert.equal(getTile10([7, 0]), Tile.OPEN);
			assert.equal(getTile10([2, 1]), Tile.WALL);
		});

		
	});
}	
