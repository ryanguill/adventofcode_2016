import fs = require('fs');

// npm run compile && node dist/2016/day1/day-1-2.js src/2016/day1/input.txt


/*
	first place you _cross_ not stop.
	input = 'R8, R4, R4, R8';

		|
		|
		|
		|
	0---x----
		|	|
		|	|
		|	|
		----|
*/

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let currentPos : Pos = [0,0];
		let currentHeading = Heading.north;

		
		let instructions = input.split(',').map(parseInstruction);
		let breadcrumbs : Pos[] = [];
		outer: for (let [direction, distance] of instructions ) {
			currentHeading += direction;
			if (currentHeading < 0) {
				currentHeading += 4;
			}
			currentHeading %= 4;

			for (let i = 1; i <= distance; i++) {
				if (currentHeading === Heading.north) {
					currentPos[1] += 1;
				} else if (currentHeading === Heading.east) {
					currentPos[0] += 1;
				} else if (currentHeading === Heading.south) {
					currentPos[1] -= 1;
				} else if (currentHeading === Heading.west) {
					currentPos[0] -= 1;
				}
				if (breadcrumbs.find(x => x[0] === currentPos[0] && x[1] === currentPos[1])) {
					console.log('found match', {currentPos, breadcrumbs});
					break outer;
				}
				breadcrumbs.push([currentPos[0], currentPos[1]]);
			}
		}
		console.log({currentPos});
		console.log({distance: Math.abs(currentPos[0]) + Math.abs(currentPos[1])});
		console.log({distance: manhattanDistance(currentPos, [0,0])});
	});
}	

function manhattanDistance (pos1 : Pos, pos2 : Pos) : number {
	return Math.abs(pos1[0] - pos2[0]) + Math.abs(pos1[1] - pos2[1]);
}

type Pos = [number, number];
enum Heading {north = 0, east = 1, south = 2, west = 3};
type turn = -1 | 1;
type instruction = [turn, number];

function parseInstruction (input : string) : instruction {
	input = input.trim();
	let directionChar = input.substring(0, 1);
	let distance = input.replace(directionChar, '');
	let direction : turn = (directionChar === 'R' ? 1 : -1);
	return [direction, Number(distance)];
}