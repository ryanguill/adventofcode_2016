import fs = require('fs');

//npm run compile && node dist/2016/day1/day-1-1.js src/2016/day1/input.txt

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let currentPos : Pos = [0,0];
		let currentHeading = Heading.north;
		
		let instructions = input.split(',').map(parseInstruction);
		for (let [direction, distance] of instructions ) {
			
			[currentHeading, currentPos] = applyInstruction(currentPos, currentHeading, direction, distance);
			//console.log({direction, distance, currentHeading, currentPos});
			console.log({currentPos});
		}
		console.log({currentPos});
		console.log({distance: Math.abs(currentPos[0]) + Math.abs(currentPos[1])});
		console.log({distance: manhattanDistance(currentPos, [0,0])});
	});
}	

function manhattanDistance (pos1 : Pos, pos2 : Pos) : number {
	return Math.abs(pos1[0] - pos2[0]) + Math.abs(pos1[1] - pos2[1]);
}


type Pos = [number, number];
enum Heading {north = 0, east = 1, south = 2, west = 3};
type turn = -1 | 1;
type instruction = [turn, number];

function applyInstruction (currentPos: Pos, currentHeading : Heading, direction: turn, distance : number) : [Heading, Pos] {
	currentHeading += direction;
	if (currentHeading < 0) {
		currentHeading += 4;
	}
	currentHeading %= 4;

	if (currentHeading === Heading.north) {
		currentPos[1] += distance;	
	} else if (currentHeading === Heading.east) {
		currentPos[0] += distance;
	} else if (currentHeading === Heading.south) {
		currentPos[1] -= distance;
	} else if (currentHeading === Heading.west) {
		currentPos[0] -= distance;
	}
	return [currentHeading, currentPos];
}

function parseInstruction (input : string) : instruction {
	input = input.trim();
	let directionChar = input.substring(0, 1);
	let distance = input.replace(directionChar, '');
	let direction : turn = (directionChar === 'R' ? 1 : -1);
	return [direction, Number(distance)];
}