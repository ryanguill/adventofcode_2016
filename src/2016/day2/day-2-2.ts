import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');



// npm run compile && node dist/2016/day2/day-2-2.js src/2016/day2/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day2/day-2-2.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let lines = input.split('\n');
		let currentPos : Pos = [1, 1];
		let currentKey : Key;
		let keys : Key[] = [];
		lines.map(s => s.split('')).forEach(function (instructionSet: Dir[]) {
			//console.log(instructionSet);
			instructionSet.forEach(function(dir) {
				[currentPos, currentKey] = move(currentPos, dir);
				//console.log({dir, currentPos, currentKey});
			});
			keys.push(currentKey);
		});
		console.log({keys});
	});
}

export type Pos = [number, number];
export type Dir = 'L' | 'R' | 'U' | 'D';
export type Key = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 'A' | 'B' | 'C' | 'D' | null;

export function clonePos (pos : Pos) : Pos {
	return [pos[0], pos[1]];
}

export function move (currentPos: Pos, dir: Dir) : [Pos, Key] {
	const prevPosition = clonePos(currentPos);

	if (dir === 'L') {
		currentPos[1] = Math.max(0, currentPos[1] - 1);
	} else if (dir === 'R') {
		currentPos[1] = Math.min(4, currentPos[1] + 1);
	} else if (dir === 'U') {
		currentPos[0] = Math.max(0, currentPos[0] - 1);
	} else if (dir === 'D') {
		currentPos[0] = Math.min(4, currentPos[0] + 1);
	}

	let key = getKeyAtPos(currentPos);
	if (key === null) {
		currentPos = prevPosition;
		key = getKeyAtPos(currentPos);
	}

	return [clonePos(currentPos), key];
}

export function getKeyAtPos (pos : Pos) : Key {
	let n = null;
	let pad : Key[][] = [
						[n,n,1,n,n],
						[n,2,3,4,n],
						[5,6,7,8,9],
						[n,'A','B','C',n],
						[n,n,'D',n,n],
						];
						
	return pad[pos[0]][pos[1]];
}

/*
===============================================================================
UNIT TESTS
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('clonePos should return a new pos', async function () {
			let p : Pos = [0,0];
			let c = clonePos(p);
			assert.typeOf(c, 'array');
			assert.lengthOf(c, 2, 'should be a 2-tuple');
			assert.equal(c[0], 0);
			assert.equal(c[1], 0);
			
			p = [1,1];
			assert.typeOf(c, 'array');
			assert.lengthOf(c, 2, 'should be a 2-tuple');
			assert.equal(c[0], 0);
			assert.equal(c[1], 0);
		});

		test('getKeyAtPos', async function () {

			assert.equal(getKeyAtPos([0,0]), null);
			assert.equal(getKeyAtPos([0,2]), 1);
			assert.equal(getKeyAtPos([0,4]), null);
			assert.equal(getKeyAtPos([1,0]), null);
			assert.equal(getKeyAtPos([1,2]), 3);

		});

		test('move', async function () {

			let [[x,y], key] = move([2, 2], 'U');
			assert.equal(x, 1);
			assert.equal(y, 2);
			assert.equal(key, 3);

			[[x,y], key] = move([0, 0], 'U');
			assert.equal(x, 0);
			assert.equal(y, 0);
			assert.equal(key, null);

			[[x,y], key] = move([0, 0], 'L');
			assert.equal(x, 0);
			assert.equal(y, 0);
			assert.equal(key, null);

			[[x,y], key] = move([0, 4], 'U');
			assert.equal(x, 0);
			assert.equal(y, 4);
			assert.equal(key, null);

			[[x,y], key] = move([0, 4], 'R');
			assert.equal(x, 0);
			assert.equal(y, 4);
			assert.equal(key, null);

			[[x,y], key] = move([4, 0], 'D');
			assert.equal(x, 4);
			assert.equal(y, 0);
			assert.equal(key, null);

			[[x,y], key] = move([4, 0], 'L');
			assert.equal(x, 4);
			assert.equal(y, 0);
			assert.equal(key, null);

			[[x,y], key] = move([4, 4], 'D');
			assert.equal(x, 4);
			assert.equal(y, 4);
			assert.equal(key, null);

			[[x,y], key] = move([4, 4], 'R');
			assert.equal(x, 4);
			assert.equal(y, 4);
			assert.equal(key, null);

		});
	});
}	
