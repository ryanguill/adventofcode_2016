import fs = require('fs');

//npm run compile && node dist/2016/day2/day-2-1.js src/2016/day2/input.txt
//64956 too low

if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {

		let lines = input.split('\n');
		let currentPos : Pos = [1, 1];
		let currentKey : Key;
		let keys : Key[] = [];
		lines.map(s => s.split('')).forEach(function (instructionSet: Dir[]) {
			//console.log(instructionSet);
			instructionSet.forEach(function(dir) {
				[currentPos, currentKey] = move(currentPos, dir);
				//console.log({dir, currentPos, currentKey});
			});
			keys.push(currentKey);
		});
		console.log(keys);

	});
}	

type Pos = [number, number];
type Dir = 'L' | 'R' | 'U' | 'D';
type Key = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

function clonePos (pos : Pos) : Pos {
	return [pos[0], pos[1]];
}

function move (currentPos: Pos, dir: Dir) : [Pos, Key] {
	let pad : Key[][] = [
						[1,2,3],
						[4,5,6],
						[7,8,9]
						];

	if (dir === 'L') {
		currentPos[1] = Math.max(0, currentPos[1] - 1);
	} else if (dir === 'R') {
		currentPos[1] = Math.min(2, currentPos[1] + 1);
	} else if (dir === 'U') {
		currentPos[0] = Math.max(0, currentPos[0] - 1);
	} else if (dir === 'D') {
		currentPos[0] = Math.min(2, currentPos[0] + 1);
	}
	return [clonePos(currentPos), pad[currentPos[0]][currentPos[1]]];
}