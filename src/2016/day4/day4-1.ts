import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');

// npm run compile && node dist/2016/day4/day4-1.js src/2016/day4/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day4/day4-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let sectorIds = input.split('\n')
			.filter(validateChecksum)
			.map(extractSectorID)
			.reduce(sum, 0);
		console.log(sectorIds);
	});
}

function extractSectorID (input : string) : number {
	let sectorString = input.substring(input.lastIndexOf('-') + 1, input.indexOf('['));
	return Number(sectorString);
}

function validateChecksum (input : string) : boolean {
	//get the checksum first
	let [checksum = null] = input.match(/\[(.*?)\]/g) || [];
	let toHash = input.substring(0, input.lastIndexOf('-')).split('-').join('');
	if (checksum === null) {
		throw 'Invalid input';
	} else {
		checksum = checksum.substring(1,6);
	}

	return checksum === generateChecksum(toHash);
}

type Occurance = {
	char: string;
	count: number;
};

function generateChecksum (input : string) : string {
	let chars = input.split('');
	let occurrances : Occurance[] = [];
	for (let char of chars) {
		let occurance = occurrances.find(o => o.char === char);
		if (occurance) {
			occurance.count++;
		} else {
			occurrances.push({char: char, count: 1});
		}
	}

	occurrances.sort(function (a, b) {
		return b.count - a.count || a.char.localeCompare(b.char);
	});
	
	return take(5, occurrances).map(pluck('char')).join('');
}

//currying signatures
function take<T> (count : number) : (x : T[]) => T[];
function take<T> (count : number, input : T[]) : T[];
function take<T> (count : number, input? : T[]) {
	if (!input) {
		return (x : T[]) : T[] => take(count, x);
	}
	return input.slice(0, Math.max(0, count));
}

//currying signatures
function pluck (key : string) : (x : Object) => any;
function pluck (key : string, input : {[key: string]: any}) : any;
function pluck (key : string, input? : {[key: string]: any}) : any | undefined {
	if (!input) {
		return function (input : {[key: string]: any}) : any | undefined {
			return pluck(key, input);
		};
	}
	if (input.hasOwnProperty(key)) {
		return input[key];
	}
	return undefined;
}

function sum (a : number, b : number) {
	return a + b;
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('validateChecksum', async function () {
			/*
			aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
			a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
			not-a-real-room-404[oarel] is a real room.
			totally-real-room-200[decoy] is not.
			*/
			assert.isTrue(validateChecksum('aaaaa-bbb-z-y-x-123[abxyz]'));
			assert.isTrue(validateChecksum('a-b-c-d-e-f-g-h-987[abcde]'));
			assert.isTrue(validateChecksum('not-a-real-room-404[oarel]'));
			assert.isFalse(validateChecksum('totally-real-room-200[decoy]'));
		});

		test('extractSectorID', async function () {
			/*
			aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
			a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
			not-a-real-room-404[oarel] is a real room.
			totally-real-room-200[decoy] is not.
			*/
			assert.equal(extractSectorID('aaaaa-bbb-z-y-x-123[abxyz]'), 123);
			assert.equal(extractSectorID('a-b-c-d-e-f-g-h-987[abcde]'), 987);
			assert.equal(extractSectorID('not-a-real-room-404[oarel]'), 404);
			assert.equal(extractSectorID('totally-real-room-200[decoy]'), 200);
		});

		
	});
}	
