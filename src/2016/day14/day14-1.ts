import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


import {md5Hash} from '../utils/utils';

// time (npm run compile && node dist/2016/day14/day14-1.js src/2016/day14/input.txt && say done)
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day14/day14-1.js

// 1879 too low
// 23981 too high
/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	run('cuanljph');
}

function run (puzzleInput : string) {

	let keys : number[] = [];

	let keyIndex = -1;
	
	while (keys.length < 64) {
		keyIndex += 1;
		//console.log({keyIndex, l: keys.length})
		let result = md5ContainsTriple(puzzleInput, keyIndex);
		if (result.success) {
			for (let i = 1; i < 1001; i++) {
				let fiveResult = md5ContainsQuintuple(puzzleInput, keyIndex + i, result.char);
				if (fiveResult.success) {
					keys.push(keyIndex);
					//console.log(result, fiveResult, keyIndex, i, keys.length);
					break;
				}
			}
		} else {
			continue;
		}
	}

	console.log({answer: keys.reverse()[0]});

}

type ResultSuccess = {
	success: true;
	key: string;
	char: string;
};

type ResultFail = {
	success: false;
};

type Result = ResultSuccess | ResultFail;

function md5ContainsTriple (prefix : string, index : number) : Result {
	const hash = md5Hash(prefix + index.toString());
	let re = /(.)\1{2}/;
	const match = hash.match(re);
	if (match === null) {
		return {
			success: false
		};
	} else {
		return {
			success: true,
			key: hash,
			char: match[1]
		};
	}
}

function md5ContainsQuintuple (prefix : string, index : number, char : string) : Result {
	const hash = md5Hash(prefix + index.toString());
	let re = `${char}{5}`;
	const match = hash.match(re);
	if (match === null) {
		return {
			success: false
		};
	} else {
		return {
			success: true,
			key: hash,
			char
		};
	}
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('md5ContainsRepeat', async function () {
			let result = md5ContainsTriple('abc', 18);
			if (result.success) {
				assert.equal(result.char, '8');
			} else {
				assert.fail();
			}

			result = md5ContainsTriple('abc', 19);
			if (result.success) {
				assert.fail();
			}

			result = md5ContainsTriple('abc', 39);
			if (result.success) {
				assert.equal(result.char, 'e');
			} else {
				assert.fail();
			}
			console.log(md5ContainsQuintuple('abc', 816, 'e'));

		});

		
	});
}	
