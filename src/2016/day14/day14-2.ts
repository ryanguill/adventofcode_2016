import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


import {md5HashStretched} from '../utils/utils';

// time (npm run compile && node dist/2016/day14/day14-2.js src/2016/day14/input.txt && say done)
// time (npm run compile && node --prof dist/2016/day14/day14-2.js src/2016/day14/input.txt && say done)
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day14/day14-2.js

// 1879 too low
// 23981 too high
/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	run('cuanljph');
	//run('abc');
}

function run (puzzleInput : string) {

	let keys : number[] = [];
	let hashes : string[] = [];

	function getHash(index : number) : string {
		if (hashes.length < index + 1) {
			hashes[index] = md5HashStretched(puzzleInput + index.toString(), 2017);
		}
		return hashes[index];
	}

	let keyIndex = -1;
	
	while (keys.length < 64) {
		keyIndex += 1;
		//console.log({keyIndex, l: keys.length})
		
		let result = md5ContainsTriple(getHash(keyIndex));
		if (result.success) {
			for (let i = 1; i < 1001; i++) {
				let fiveResult = md5ContainsQuintuple(getHash(keyIndex + i), result.char);
				if (fiveResult.success) {
					keys.push(keyIndex);
					//console.log(result, fiveResult, keyIndex, i, keys.length);
					break;
				}
			}
		} else {
			continue;
		}
	}

	console.log({answer: keys.reverse()[0]});

}



type ResultSuccess = {
	success: true;
	key: string;
	char: string;
};

type ResultFail = {
	success: false;
};

type Result = ResultSuccess | ResultFail;

function md5ContainsTriple (hash : string) : Result {
	
	let re = /(.)\1{2}/;
	const match = hash.match(re);
	if (match === null) {
		return {
			success: false
		};
	} else {
		return {
			success: true,
			key: hash,
			char: match[1]
		};
	}
}

function md5ContainsQuintuple (hash : string, char : string) : Result {
	let re = `${char}{5}`;
	const match = hash.match(re);
	if (match === null) {
		return {
			success: false
		};
	} else {
		return {
			success: true,
			key: hash,
			char
		};
	}
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('md5ContainsRepeat', async function () {
			let hashes : string[] = [];
			let puzzleInput = 'abc';

			function getHash(index : number) : string {
				if (hashes.length < index) {
					hashes[index] = md5HashStretched(puzzleInput + index.toString(), 2017);
				}
				return hashes[index];
			}
			let result = md5ContainsTriple(getHash(5));
			if (result.success) {
				assert.equal(result.char, '2');
			} else {
				assert.fail();
			}

			// result = md5ContainsTriple('abc', 19);
			// if (result.success) {
			// 	assert.fail();
			// }

			// result = md5ContainsTriple('abc', 39);
			// if (result.success) {
			// 	assert.equal(result.char, 'e')
			// } else {
			// 	assert.fail();
			// }
			// console.log(md5ContainsQuintuple('abc', 816, 'e'));

		});

		
	});
}	
