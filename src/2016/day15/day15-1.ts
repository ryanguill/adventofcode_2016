import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// time (npm run compile && node dist/2016/day15/day15-1.js src/2016/day15/input.txt)
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day15/day15-1.js

// part1 2.901s
// part2 3.315s

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	const input = `Disc #1 has 17 positions; at time=0, it is at position 5.
Disc #2 has 19 positions; at time=0, it is at position 8.
Disc #3 has 7 positions; at time=0, it is at position 1.
Disc #4 has 13 positions; at time=0, it is at position 7.
Disc #5 has 5 positions; at time=0, it is at position 1.
Disc #6 has 3 positions; at time=0, it is at position 0.`;

	const input2 = `Disc #1 has 17 positions; at time=0, it is at position 5.
Disc #2 has 19 positions; at time=0, it is at position 8.
Disc #3 has 7 positions; at time=0, it is at position 1.
Disc #4 has 13 positions; at time=0, it is at position 7.
Disc #5 has 5 positions; at time=0, it is at position 1.
Disc #6 has 3 positions; at time=0, it is at position 0.
Disc #7 has 11 positions; at time=0, it is at position 0.
`;

	let time = 0;
	const discs = parseInput(input);
	while (true) {
		if (discs.every(d => evaluateDiscAtTime(d, time))) {
			console.log({answer: time});
			break;
		}
		time += 1;
	}
}

type Disc = {
	positions : number,
	initialPosition : number,
	index: number;
};

function parseInput (input : string) : Disc[] {
	return input.split('\n').map( (line, index) => {
		let [, positions, , initialPosition] = line.match(/(\d+)/g) || [undefined, -1, undefined, -1];
		return {
			positions: Number(positions),
			initialPosition: Number(initialPosition),
			index: index + 1,
		};
	});
}

function evaluateDiscAtTime (disc : Disc, dropTime : number) : boolean {
	return (dropTime + disc.initialPosition + disc.index) % disc.positions === 0;
}


/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('parseInput', async function () {
			assert.deepEqual(parseInput(`Disc #1 has 5 positions; at time=0, it is at position 4.
Disc #2 has 2 positions; at time=0, it is at position 1.`),
			[
				{positions:5, initialPosition: 4, index: 1},
				{positions:2, initialPosition: 1, index: 2},
			]);
		});

		test('evaluateDiscAtTime', async function () {
			assert.isTrue(evaluateDiscAtTime({positions: 5, initialPosition: 4, index: 1}, 0), '0');
			assert.isFalse(evaluateDiscAtTime({positions: 5, initialPosition: 4, index: 1}, 1), '1');
			assert.isFalse(evaluateDiscAtTime({positions: 5, initialPosition: 4, index: 1}, 2), '2');
			assert.isTrue(evaluateDiscAtTime({positions: 5, initialPosition: 4, index: 1}, 5), '6');
		});

		
	});
}	
