import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day7/day7-2.js src/2016/day7/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day7/day7-2.js


/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let answer = input.split('\n').map(trim).filter(supportsSSL).length;
		console.log({answer});
	});
}

function trim (input : string)  {
	return input.trim();
}

interface HasLength {
	length: number;
}

function isEmpty<T extends HasLength> (input : T) : boolean {
	return input.length === 0;
}

type F = (...args: any[]) => any;
function not (input : F) : F;
function not (input : boolean) : boolean;
function not (input : F | boolean) : F | boolean {
	if (typeof input === 'function') {
		return function () {
			return !input.apply(null, arguments);
		};
	} else {
		return !input;
	}	
}

function supportsSSL (input : string) {
	const supernets = getSupernetSequences(input).map(findABAs);
	const hypernets = getHypernetSequences(input);
	const result = supernets.filter(function (ABAs) {
		//find the matches in the hypernets
		return ABAs.some(function (ABA) {
			let [a, b] = ABA;
			return hypernets.find(x => x.indexOf([b, a, b].join('')) !== -1) !== undefined;
		});
		
	});
	return result.length > 0;
}

function getSupernetSequences (input : string) : string[] {
	//theres a better way thats escaping me right now than the filter ive got
	return input.replace(/\[.+?\]/g, '|').split('|').filter(not(isEmpty));
}

function getHypernetSequences (input : string) : string[] {
	return (input.match(/\[.+?\]/g) || []).map(x => x.replace(/[\[\]]/g, '')).filter(not(isEmpty));
}


function findABAs (input : string) {
	let data = input.split('');
	let output = [];
	for (let i = 0; i < data.length; i++) {
		let slice = data.slice(i, i + 3);
		if (slice.length !== 3) {
			break;
		}
		let [a, b, c] = slice;
		if (a === c && a !== b) {
			output.push(slice.join(''));
		}
	}
	return output;
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('findABAs', async function () {
			assert.deepEqual(findABAs('aba'), ['aba']);
			assert.deepEqual(findABAs('abaxyx'), ['aba', 'xyx']);
			assert.deepEqual(findABAs('xxxabayyy'), ['aba']);
			assert.deepEqual(findABAs('xxxabayyyxxxdedyyyxxxfgfxxx'), ['aba', 'ded', 'fgf']);
		});	

		test('getSupernetSequences', async function () {
			assert.deepEqual(getSupernetSequences('abc'), ['abc']);
			assert.deepEqual(getSupernetSequences('abc[def]'), ['abc']);
			assert.deepEqual(getSupernetSequences('abc[def]ghi'), ['abc', 'ghi']);
		});	

		test('getHypernetSequences', async function () {
			assert.deepEqual(getHypernetSequences('abc'), []);
			assert.deepEqual(getHypernetSequences('abc[def]'), ['def']);
			assert.deepEqual(getHypernetSequences('abc[def]ghi'), ['def']);
			assert.deepEqual(getHypernetSequences('abc[def]ghi[jkl]'), ['def','jkl']);
			assert.deepEqual(getHypernetSequences('abc[def]ghi[jkl]mno'), ['def','jkl']);
		});

		test('supportsSSL', async function () {
			assert.isTrue(supportsSSL('xxxabaxxx[xxxbabxxx]xxxcdc'));
			assert.isTrue(supportsSSL('aaa[kek]eke'));
			assert.isFalse(supportsSSL('aaa[eke]eke'));
		});

		test('isEmpty', async function () {
			const notIsEmpty = not(isEmpty);
			
			assert.isTrue(isEmpty([]));
			assert.isFalse(isEmpty([1]));
			assert.isTrue(isEmpty(''));
			assert.isFalse(isEmpty('x'));

			assert.isFalse(notIsEmpty([]));
			assert.isTrue(notIsEmpty([1]));
			assert.isFalse(notIsEmpty(''));
			assert.isTrue(notIsEmpty('x'));

			assert.isFalse(not(isEmpty([])));
			assert.isTrue(not(isEmpty([1])));
			assert.isFalse(not(isEmpty('')));
			assert.isTrue(not(isEmpty('x')));
			
		});
		
	});
}	
