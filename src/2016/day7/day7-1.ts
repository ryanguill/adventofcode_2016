import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day7/day7-1.js src/2016/day7/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day7/day7-1.js
//67 too low
//133 too high

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		let answer = input.split('\n').map(trim).filter(supportsTLS).length;
		console.log({answer});
	});
}

function hasAbba (input : string) : boolean {
	let data = input.split('');
	for (let i = 0; i < data.length; i++) {
		let slice = data.slice(i, i + 4);
		if (slice.length !== 4) {
			return false;
		}
		let [a, b, c, d] = slice;
		if (a === d && b === c && a !== b) {
			return true;
		}
	}
	return false;
}

function isQualified (input : string) : boolean {
	let data = input.replace(/\[.+?\]/g, '|').split('|');
	return data.some(hasAbba);
}

function tail<T> (input : T[]) : T[] {
	return input.slice(1, input.length);
}

function head<T> (input : T[]) : T {
	return input[0];
}

function isDisqualified (input : string) : boolean {
	//let data = tail(input.split(']')).map(x => head(x.split(']')));
	let data = input.match(/\[.+?\]/g) || [];
	return data.map(x => x.replace(/[\[\]]/g, '')).some(hasAbba);
}

function trim (input : string)  {
	return input.trim();
}

function supportsTLS (input : string) {
	return isQualified(input) && !isDisqualified(input);
}

/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('isQualified', async function () {
			let testIndex = 0;
			assert.isTrue(isQualified('abba[mnop]qrst'), `${++testIndex}`);
			assert.isTrue(isQualified('abcd[bddb]xyyx'), `${++testIndex}`);
			assert.isFalse(isQualified('aaaa[qwer]tyui'), `${++testIndex}`);
			assert.isTrue(isQualified('ioxxoj[asdfgh]zxcvbn'), `${++testIndex}`);
			assert.isFalse(isQualified('iniqqqqprdeznwhr[arooglolfjgprfrbhbm]sczcmpftuhbaagwgedq[yutgzaqyxntjxoglmb]vswbhlspwfulowkif'), `${++testIndex}`);
		});


		test('isDisqualified', async function () {
			let testIndex = 0;
			assert.isFalse(isDisqualified('abba[mnop]qrst'), `${++testIndex}`);
			assert.isTrue(isDisqualified('abcd[bddb]xyyx'), `${++testIndex}`);
			assert.isFalse(isDisqualified('aaaa[qwer]tyui'), `${++testIndex}`);
			assert.isFalse(isDisqualified('ioxxoj[asdfgh]zxcvbn'), `${++testIndex}`);
			assert.isTrue(isDisqualified('aaaa[qwer]tyui[abba]xyz'), `${++testIndex}`);
			assert.isTrue(isDisqualified('aaaa[qwer]tyui[xxxxxxxabbaxxxxxxx]xyz'), `${++testIndex}`);
			assert.isTrue(isDisqualified('ngscsmmvttxjxoaniu[wsfnnfnkwmjxjql]ijtqswaevexzcmgime'), `${++testIndex}`);
		});


		test('supportsTLS', async function () {
			let testIndex = 0;
			assert.isTrue(supportsTLS('abba[mnop]qrst'), `${++testIndex}`);
			assert.isFalse(supportsTLS('abcd[bddb]xyyx'), `${++testIndex}`);
			assert.isFalse(supportsTLS('aaaa[qwer]tyui'), `${++testIndex}`);
			assert.isTrue(supportsTLS('ioxxoj[asdfgh]zxcvbn'), `${++testIndex}`);
			assert.isFalse(supportsTLS('aaaa[qwer]tyui[abba]xyz'), `${++testIndex}`);
			assert.isFalse(supportsTLS('aaaa[qwer]tyui[xxxxxxxabbaxxxxxxx]xyz'), `${++testIndex}`);
			assert.isFalse(supportsTLS('frfongnaecavmrhj[uuptwvcqozxnglsd]kwpovfwuhmqjkog[phtrgysxaltvbohmv]abhuahonmbhsmfglltm'), `${++testIndex}`);
			assert.isFalse(supportsTLS('rwcjoknjvyufkrp[hsqrwhdlwrtlthp]ijezyezjdrhzgltliq'), `${++testIndex}`);
			assert.isFalse(supportsTLS('ngscsmmvttxjxoaniu[wsfnnfnkwmjxjql]ijtqswaevexzcmgime'), `${++testIndex}`);
			assert.isTrue(supportsTLS('rxpusykufgqujfe[rypwoorxdemxffui]cvvcufcqmxoxcphp[witynplrfvquduiot]vcysdcsowcxhphp[gctucefriclxaonpwe]jdprpdvpeumrhokrcjt'), `${++testIndex}`);
			assert.isFalse(supportsTLS('unfjgussbjxzlhopoqg[ppdnqkiuooukdmbqlo]flfiieiitmettblfln'), `${++testIndex}`);
			assert.isFalse(supportsTLS('iniqqqqprdeznwhr[arooglolfjgprfrbhbm]sczcmpftuhbaagwgedq[yutgzaqyxntjxoglmb]vswbhlspwfulowkif'), `${++testIndex}`);
			assert.isFalse(supportsTLS('ukuozdurxxrvljkoi[eysjyckwyiyuopa]fconkkukvvmgnvyn[nwkqsifcwlzjurruho]ryslsdfmhgesmdf'), `${++testIndex}`);
			assert.isTrue(supportsTLS('suqgbqjkbymetile[jmgtojqeprpffkogv]pnffnorgbyrwddlm[pozvbivhruebmmjwogs]fuleotnbbnpuqre'), `${++testIndex}`);
			assert.isTrue(supportsTLS('uveiuvjjvxlrlyxpc[ucbsqehsihiilfmxz]nvhvrtaxdpiqpbuyj[rhtfedxbiqutzvaucj]dxtgbmuwqxxqpfbohfb'), `${++testIndex}`);
			assert.isFalse(supportsTLS('bzkfxruybgbjsmeflp[ubqugccjulkryeh]yaldnucjnxhktrgf[jamlgglyzdlaidp]dsvhuxerhecwhjslfk[yibicfmmkiugttqi]byyytkpsforwainomd'), `${++testIndex}`);
			assert.isTrue(supportsTLS('jjgkkenzhjxreujjury[oqbhgbvyvybhznj]lzzpsybrpvmfxftasjv[fajchcbbxzyjzdmzju]jbyovxsryhenhqd'), `${++testIndex}`);
			assert.isFalse(supportsTLS('apezafwfsqifmip[kuxynugvrndkdifwhd]pixpqmsbuqspsdpqnzw[qfiqdwcdzbtidgm]otzbkrciacwchdnqa[kiblvlkmtkqjzyyv]hnucmptrhrodos'), `${++testIndex}`);
		});
		

		test('tail', async function () {
			assert.deepEqual(tail([1,2,3,4]), [2,3,4]);
			assert.deepEqual(tail([1,2,3,4,5,6]), [2,3,4,5,6]);
			assert.deepEqual(tail([1]), []);
			assert.deepEqual(tail([]), []);
		});	

		
	});
}	
