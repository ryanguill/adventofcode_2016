import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day9/day9-1.js src/2016/day9/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day9/day9-1.js

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		console.log({length: decompress(input.trim()).length});
	});
}


type Marker = {
	range: number;
	times : number;
	startIndex : number;
	markerIndex : number;
};

function getNextMarker (input : string) : Marker | null {
	const re = /\(\d+x\d+\)/g;
	const execResult = re.exec(input);
	if (execResult) {
		const markerExpression = execResult[0];
		const markerIndex = execResult.index;
		const [range, times] = markerExpression.substring(1, markerExpression.length - 1).split('x');
		return {
			range: Number(range),
			times: Number(times),
			markerIndex: markerIndex,
			startIndex: markerIndex + markerExpression.length,
		};
	}
	return null;
}

function decompress (input : string, acc : string = '') : string {
	let marker = getNextMarker(input);
	if (marker) {
		let prefix = input.substring(0, marker.markerIndex);
		let repeated = input.substring(marker.startIndex, marker.startIndex + marker.range).repeat(marker.times);
		acc += prefix + repeated;
		return decompress(input.substring(marker.startIndex + marker.range), acc);	
	} else {
		return acc += input;
	}
	
}



/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('decompress', async function () {
			assert.equal(decompress('a(2x2)cd'), 'acdcd');
			assert.equal(decompress('ADVENT'), 'ADVENT');
			assert.equal(decompress('A(1x5)BC'), 'ABBBBBC');
			assert.equal(decompress('(3x3)XYZ'), 'XYZXYZXYZ');
			assert.equal(decompress('A(2x2)BCD(2x2)EFG'), 'ABCBCDEFEFG');
			assert.equal(decompress('(6x1)(1x3)A'), '(1x3)A');
			assert.equal(decompress('X(8x2)(3x3)ABCY'), 'X(3x3)ABC(3x3)ABCY');
		});

		test('getNextMarker', async function () {
			assert.deepEqual(getNextMarker(''), null);
			assert.deepEqual(getNextMarker('abcdef'), null);
			assert.deepEqual(getNextMarker('(1x2)abc'), {range: 1, times: 2, markerIndex: 0, startIndex: 5});
			assert.deepEqual(getNextMarker('abc(123x234)abc'), {range: 123, times: 234, markerIndex: 3, startIndex: 12});
			assert.deepEqual(getNextMarker('x(2x2)(1x1)'), {range: 2, times: 2, markerIndex: 1, startIndex: 6});
		});

		
	});
}	
