import fs = require('fs');
import mocha = require('mocha');
import chai = require('chai');


// npm run compile && node dist/2016/day9/day9-2.js src/2016/day9/input.txt
// npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day9/day9-2.js

// time (npm run compile && node dist/2016/day9/day9-2.js src/2016/day9/input.txt && say done)
// real	2m33.142s
// 10943094568

/*
===============================================================================
Get Input and main function run - wont run when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1] === __filename) {
	fs.readFile(process.argv[2], 'utf8', function (err: any, input: string) {
		console.log({length: decompressLength(input.trim())});
	});
}


type Marker = {
	range: number;
	times : number;
	startIndex : number;
	markerIndex : number;
};

function getNextMarker (input : string) : Marker | null {
	const re = /\(\d+x\d+\)/;
	const execResult = re.exec(input);
	if (execResult) {
		const markerExpression = execResult[0];
		const markerIndex = execResult.index;
		const [range, times] = markerExpression.substring(1, markerExpression.length - 1).split('x');
		return {
			range: Number(range),
			times: Number(times),
			markerIndex: markerIndex,
			startIndex: markerIndex + markerExpression.length,
		};
	}
	return null;
}

function decompressLength (input : string, acc : number = 0) : number {
	let marker = getNextMarker(input);
	if (marker) {
		let prefix = input.substring(0, marker.markerIndex);
		let repeatedLen = decompressLength(input.substring(marker.startIndex, marker.startIndex + marker.range)) * marker.times;
		acc += prefix.length + repeatedLen;
		return decompressLength(input.substring(marker.startIndex + marker.range), acc);	
	} else {
		return acc += input.length;
	}
	
}



/*
===============================================================================
UNIT TESTS - only runs when unit testing
===============================================================================	
*/
if (process.argv.length > 1 && process.argv[1].includes('mocha')) {
		
	let expect = chai.expect;
	let assert = chai.assert;

	suite(__filename.split('/').reverse()[0] + ' tests', function () {

		test('decompressLength', async function () {
			assert.equal(decompressLength('a(2x2)cd'), 5);
			assert.equal(decompressLength('ADVENT'), 6);
			assert.equal(decompressLength('A(1x5)BC'), 7);
			assert.equal(decompressLength('(3x3)XYZ'), 9);
			assert.equal(decompressLength('X(8x2)(3x3)ABCY'), 20);
			assert.equal(decompressLength('(27x12)(20x12)(13x14)(7x10)(1x12)A'), 241920);
			assert.equal(decompressLength('(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN'), 445);
		});

		test('getNextMarker', async function () {
			assert.deepEqual(getNextMarker(''), null);
			assert.deepEqual(getNextMarker('abcdef'), null);
			assert.deepEqual(getNextMarker('(1x2)abc'), {range: 1, times: 2, markerIndex: 0, startIndex: 5});
			assert.deepEqual(getNextMarker('abc(123x234)abc'), {range: 123, times: 234, markerIndex: 3, startIndex: 12});
			assert.deepEqual(getNextMarker('x(2x2)(1x1)'), {range: 2, times: 2, markerIndex: 1, startIndex: 6});
		});

		
	});
}	
