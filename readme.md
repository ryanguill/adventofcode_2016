[![build status](https://gitlab.com/ryanguill/adventofcode_2016/badges/master/build.svg)](https://gitlab.com/ryanguill/adventofcode_2016/commits/master)

example of how to run a day `npm run compile && node dist/2015/day6/day-6-2.js src/2015/day6/input.txt`

Note: this will **delete** dist/ first.  you shouldnt care.

You can run all tests with `npm test`.

You can run individual tests by using something like `npm run compile && ./node_modules/.bin/mocha --ui tdd dist/2016/day2/day-2-2.js`

If you use this setup, it wouldnt take much to get a gulp script to watch for changes and run the compile step (and/or test step) for you automatically.